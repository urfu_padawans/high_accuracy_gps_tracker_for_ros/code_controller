#ifndef RING_BUFFER_H
#define RING_BUFFER_H

#define RING_BUF_SIZE 20

struct RingBuffer
{
	char counter;
	char head;	
	char memory[RING_BUF_SIZE];
	char tail;
};

void Write_byte(struct RingBuffer* ringbuff, char byte); // write one byte in a ring buffer

char Read_byte(struct RingBuffer* ringbuff); // read one byte from a ring buffer

void Buffer_clear(struct RingBuffer* ringbuff); // clear a ring buffer

char Buffer_status(struct RingBuffer* ringbuff); // get ring buffer status

#endif 