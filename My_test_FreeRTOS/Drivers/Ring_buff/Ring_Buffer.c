﻿#include "Ring_Buffer.h"

void Write_byte(struct RingBuffer* ringbuff, char byte) // write one byte in a ring buffer
{
	if (ringbuff->counter < RING_BUF_SIZE)
	{
		ringbuff->memory[ringbuff->tail] = byte;
		ringbuff->counter++;
		ringbuff->tail++;
		if (ringbuff->tail == RING_BUF_SIZE) ringbuff->tail = 0;
	}
}

char Read_byte(struct RingBuffer* ringbuff) // read one byte from ring buffer
{
	char data = 0;
	if (ringbuff->counter > 0)
	{
		data = ringbuff->memory[ringbuff->head];
		ringbuff->head++;
		ringbuff->counter--;
		if (ringbuff->head == RING_BUF_SIZE) ringbuff->head = 0;
	}
	return data;
}

void Buffer_clear(struct RingBuffer* ringbuff) // clear a ring buffer
{
	for (int i = 0; i < RING_BUF_SIZE; i++) ringbuff->memory[i] = 0x00;
	ringbuff->head = ringbuff->tail = ringbuff->counter = 0;
}

char Buffer_status(struct RingBuffer* ringbuff) // get ring buffer status
{
	if ( (ringbuff->head != ringbuff->tail) || (ringbuff->head == ringbuff->tail == RING_BUF_SIZE) ) return 1; // the ring buffer is full or not empry
	else return 0; // the ring buffer is empry
}


