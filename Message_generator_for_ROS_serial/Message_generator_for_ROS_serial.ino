#include <string.h>

#define delay_ms 500
#define port_speed 57600

unsigned long counter = 0;

void setup() {
  Serial. begin(port_speed);
}

void loop() {
  Serial. print("Counter is ");
  Serial. println(counter);
  counter++;
  delay(delay_ms);
}
